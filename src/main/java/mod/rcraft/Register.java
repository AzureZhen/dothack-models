package mod.rcraft;

import mod.rcraft.bladebrandier.Blade;
import mod.rcraft.blocks.BlockBase;
import mod.rcraft.blocks.BlockGate;
import mod.rcraft.dualgunner.Gunner;
import mod.rcraft.edgepunisher.Edge;
import mod.rcraft.flickreaper.Flick;
import mod.rcraft.harvestcleric.Harvest;
import mod.rcraft.lordpartizan.Lord;
import mod.rcraft.macabredancer.Macabre;
import mod.rcraft.shadowwarlock.Shadow;
import mod.rcraft.steamgunner.Steam;
import mod.rcraft.tribalgrappler.Tribal;
import mod.rcraft.twinblade.Twin;
import net.minecraft.item.Item;

public class Register {

	public static final Item bladeBrandierItem =  new Blade();
	public static final Item dualGunnerItem =  new Gunner();
	public static final Item edgePunisherItem =  new Edge();
	public static final Item flickReaperItem =  new Flick();
	public static final Item harvestClericItem =  new Harvest();
	public static final Item lordPartizanItem =  new Lord();
	public static final Item macabreDancerItem =  new Macabre();
	public static final Item shadowWarlockItem =  new Shadow();
	public static final Item steamGunnerItem =  new Steam();
	public static final Item tribalGrapplerItem =  new Tribal();
	public static final Item twinBladeItem =  new Twin();
	
	public static final BlockBase gateBlock =  new BlockGate();
	
	public static final Item[] itemList = new Item[] {
			bladeBrandierItem, dualGunnerItem, edgePunisherItem, flickReaperItem, 
			harvestClericItem, lordPartizanItem, macabreDancerItem, shadowWarlockItem, 
			steamGunnerItem, tribalGrapplerItem, twinBladeItem
	};
	
	public static final BlockBase[] blockList = new BlockBase[] {
			gateBlock
	};
	
	public static void registerRenderers(RcraftMod ins) {
		ins.rcraft_1.registerRenderers();
	}
}
