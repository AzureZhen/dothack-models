package mod.rcraft;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.IFuelHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.client.model.obj.OBJLoader;

import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.entity.player.EntityPlayer;

import java.util.Random;

import mod.rcraft.blocks.*;
import mod.rcraft.dualgunner.*;
import mod.rcraft.edgepunisher.*;
import mod.rcraft.flickreaper.*;
import mod.rcraft.harvestcleric.*;
import mod.rcraft.lordpartizan.*;
import mod.rcraft.shadowwarlock.*;
import mod.rcraft.tabs.*;
import mod.rcraft.tribalgrappler.*;
import mod.rcraft.twinblade.*;
import mod.rcraft.bladebrandier.*;
import mod.rcraft.steamgunner.*;
import mod.rcraft.macabredancer.*;

@Mod(modid = RcraftMod.MODID, version = RcraftMod.VERSION)
public class RcraftMod {

	public static final String MODID = "rcraft";
	public static final String VERSION = "1.5";

	@Instance(MODID)
	public static RcraftMod instance;
	
	BlockGate rcraft_1 = new BlockGate();
	
	@EventHandler
	public void load(FMLInitializationEvent event) {
		
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());

		rcraft_1.load(event);
		Register.registerRenderers(this);

	}

	@EventHandler
	public void serverLoad(FMLServerStartingEvent event) {

		rcraft_1.serverLoad(event);
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		if (event.getSide() == Side.CLIENT) {
			OBJLoader.INSTANCE.addDomain("rcraft");
		}
		rcraft_1.instance = this.instance;
		rcraft_1.preInit(event);
	}

	public static class GuiHandler implements IGuiHandler {
		@Override
		public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
			return null;
		}

		@Override
		public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
			return null;
		}
	}

}