package mod.rcraft.blocks;

import mod.rcraft.RcraftMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;

public class BlockBase extends Block {

	public final String name;
	public final ItemBlock item;
	
	public BlockBase (String name, Material material) {
		super(material);
		this.name = name;
		this.setUnlocalizedName(RcraftMod.MODID + "." + name);
        this.setRegistryName(RcraftMod.MODID + ":" + name);
        this.setCreativeTab(null);
        this.item = new ItemBlock(this);
        item.setUnlocalizedName(this.getUnlocalizedName());
        item.setRegistryName(this.getRegistryName());
	}
}
