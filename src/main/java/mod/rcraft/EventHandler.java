package mod.rcraft;

import java.util.ArrayList;
import java.util.List;

import mod.rcraft.blocks.BlockBase;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;

@Mod.EventBusSubscriber

public class EventHandler {

	 @SubscribeEvent
	 public static void registerBlocks(RegistryEvent.Register<Block> event) {
	    IForgeRegistry<Block> registry = event.getRegistry();
	        
	   for (Block block : Register.blockList) {
	       registry.register(block);
	   }
	}
	
	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        IForgeRegistry<Item> registry = event.getRegistry();

        for (Item item : Register.itemList) {
 	       registry.register(item);
 	   }
    }
	
	@SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
		for (BlockBase block : Register.blockList) {
			
			NonNullList<ItemStack> variants = NonNullList.create();
			block.item.getSubItems(null, variants);
			
			for (ItemStack stack : variants) {
				ModelLoader.setCustomModelResourceLocation(block.item, stack.getMetadata(), 
				new ModelResourceLocation(block.getRegistryName(), String.format("type=%n", stack.getMetadata()+1)));
			}
		}
    }
}
