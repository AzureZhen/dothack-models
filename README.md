# README #

*All rights belong to their respective owners*

### What is this repository for? ###

The source of how I load .hack models into Minecraft. Currently Last Recode/GU weapons done. IMOQ next.

### Credit ###
Big code change thanks to @Galaxtone. 

### Currently Added ###
Added in R1/R2 Chaos Gate.  
Added in R2 Field Gate.  
Added in 11 Tribal Grapplers Weapons.  
Added in 20 Broadsword Weaons.  
Added in 9 Harvest Cleric Weapons.  
Added in 19 Blade Brandier Weapons.  
Added in 14 Shadow Warlock Weapons.  
Added in 19 Steam Gunner Weapons.  
Added in 5 Macabre Dancer Weapons.  
Added in 3 Dual Gunner Weapons.  
Added in 7 Scythe Weapons.  
Added in 9 Lance Weapons.  
Added in 25 Twin Blade Models.  
